﻿using UnityEditor;
using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace LabManager
{
    [System.Serializable]
    public class TolpologyEditor:Editor
    {
        public Dictionary<string,ModuleEditor> moduleEditors = new Dictionary<string, ModuleEditor>();
        public Dictionary<string, LinkEditor> linkEditors = new Dictionary<string, LinkEditor>();
        public event EventHandler LinkAdded;
        public event EventHandler LinkRemoved;
        private Dictionary<string, Dictionary<int,bool>> LinkColors = new Dictionary<string, Dictionary<int,bool>>();
        private Color[] colors = new Color[]
        {
            Color.black,Color.blue,Color.cyan,Color.red,Color.white,Color.magenta,Color.yellow,Color.green
        };

        Func<Link,Rect[]> GetLinkPositions;
        Param selectedIn;
        Param selectedOut;

        public TolpologyEditor()
        {
            GetLinkPositions = GetPositions;
        }

        public void AddModule(ModuleEditor editor,string key, float position)
        {
            if (key.Equals(string.Empty))
            {
                key = ModuleKey(editor.module.ID);
            }
            editor.OnInputClicked = ConnectInput;
            editor.OnOutputClicked = ConnectOutput;
            editor.OnRemove = RemoveNode;
            editor.Key = key;
            if(position > 1)
            {
                editor.Place(position);
            }
            if (!moduleEditors.ContainsKey(key))
            {
                moduleEditors.Add(key, editor);
            }
        }

        public void AddLinkEditor(Param selectedIn,Param selectedOut)
        {
            string key = LinkKey(selectedIn, selectedOut);
            Link link = new Link
            {
                Input = selectedIn,
                Output = selectedOut
            };
            LinkEditor linkEditor = new LinkEditor(link, this.GetLinkPositions, this.RemoveLink, GetLinkColor(selectedIn, selectedOut));
            if (!linkEditors.ContainsKey(key))
            {
                linkEditors.Add(key, linkEditor);
            }
            EventHandler handler = LinkAdded;
            LinkArgs args = new LinkArgs();
            args.Link = link;
            handler?.Invoke(this, args);
        }

        public void Draw()
        {
            EditorWindow mainWindow = LabManagerEditor.GetMainWindow();
            if (mainWindow == null)
            {
                return;
            }
            mainWindow.BeginWindows();
            DrawModules();
            DrawLinks();
            mainWindow.EndWindows();
            ProcessModuleEvents(Event.current);
        }

        private void DrawLinks()
        {
            try
            {
                foreach (KeyValuePair<string, LinkEditor> entry in linkEditors)
                {
                    entry.Value.Draw();
                }
            }
            catch (InvalidOperationException e)
            {

            }
        }

        private void DrawModules()
        {
            try
            {
                foreach (KeyValuePair<string, ModuleEditor> entry in moduleEditors)
                {
                    entry.Value.Draw();
                }
            }
            catch (InvalidOperationException e)
            {

            }
        }

        private void ProcessModuleEvents(Event ev)
        {
            try
            {
                foreach (KeyValuePair<string, ModuleEditor> entry in moduleEditors)
                {
                    bool guiChanged = entry.Value.ProcessEvents(ev);
                    if (guiChanged)
                    {
                        GUI.changed = true;
                    }
                }
            }
            catch (InvalidOperationException e)
            {

            }
        }

        private void ConnectInput(string module,string eventName, string param)
        {
            selectedIn = new Param{ ModuleName = module,EventName = eventName,ParamName = param};

            CreateLink();
        }

        private void ConnectOutput(string module, string eventName, string param)
        {
            selectedOut = new Param { ModuleName = module, EventName = eventName, ParamName = param };
            CreateLink();
        }

        private void CreateLink()
        {
            if (selectedIn == null || selectedOut == null || selectedIn.ModuleName.Equals(selectedOut.ModuleName))
            {
                return;
            }
            AddLinkEditor(selectedIn,selectedOut);
            selectedIn = null;
            selectedOut = null;
        }


        public void RemoveLink(Link link)
        {
            if (link == null)
            {
                return;
            }

            linkEditors.Remove(LinkKey(link));
            EventHandler handler = LinkRemoved;
            LinkArgs args = new LinkArgs();
            args.Link = link;
            handler?.Invoke(this, args);
        }

        public void RemoveNode(ModuleEditor editor)
        {
            string keyToRemove = editor.Key;
            List<String> editorLinkKeys = new List<String>();
            foreach (KeyValuePair<string, LinkEditor> entry in linkEditors)
            {
                Link link = entry.Value.Link;
                if (link.Input.ModuleName.Equals(keyToRemove) || link.Output.ModuleName.Equals(keyToRemove))
                {
                    editorLinkKeys.Add(entry.Key);
                }
            }
            moduleEditors.Remove(keyToRemove);
            foreach(String key in editorLinkKeys)
            {
                linkEditors.Remove(key);
            }
            
        }

        public class LinkArgs : EventArgs
        {
            public Link Link { get; set; }
        }

        public Rect[] GetPositions(Link link)
        {
            Rect[] positions = new Rect[2];
            ModuleEditor input = moduleEditors[link.Input.ModuleName];
            ModuleEditor output = moduleEditors[link.Output.ModuleName];
            positions[0] = input.GetInputAnchorPosition(link.Input.EventName, link.Input.ParamName);
            positions[1] = output.GetOutputAnchorPosition(link.Output.EventName, link.Output.ParamName);
            return positions;
        }

        private static string LinkKey(Param inParam, Param outParam)
        {
            return string.Format("link{0}{1}", inParam.GetName(), outParam.GetName());
        }

        private static string ColorKey(Param inParam, Param outParam)
        {
            return string.Format("c{0}{1}", inParam.ModuleName, outParam.ModuleName);
        }

        private static string LinkKey(Link link)
        {
            return string.Format("link{0}{1}", link.Input.GetName(), link.Output.GetName());
        }

        private string ModuleKey(int v)
        {
            return String.Format("module_{0}_{1}", v,moduleEditors.Count);
        }


        private Color GetLinkColor(Param selectedIn, Param selectedOut)
        {
            string colorKey = ColorKey(selectedIn, selectedOut);
            int newColorIndex = 0;
            if (!LinkColors.ContainsKey(colorKey))
            {
                LinkColors[colorKey] = new Dictionary<int, bool>();
            }
            else
            {
                for (int i = 0; i < colors.Length; i++)
                {
                    if (!LinkColors[colorKey].ContainsKey(i))
                    {
                        newColorIndex = i;
                        break;
                    }
                }
            }
            LinkColors[colorKey][newColorIndex] = true;
            Color color = colors[newColorIndex];
            return color;
        }
    }
}   
