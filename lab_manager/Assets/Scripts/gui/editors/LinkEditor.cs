﻿using UnityEngine;
using UnityEditor;
using LabManager;
using System;
using Newtonsoft.Json;

namespace LabManager
{
    [System.Serializable]
    public class LinkEditor : Editor
    {

        public Link Link;
        private Action<Link> onRemove;
        private Func<Link,Rect[]> getPositions;
        private Color color;
        private System.Random rand = new System.Random();
       

        public LinkEditor()
        {
            
        }
        public LinkEditor(Link link, Func<Link,Rect[]> getPositions,Action<Link> onRemove,Color color)
        {
            this.Link = link;
            this.onRemove = onRemove;
            this.getPositions = getPositions;
            this.color = color;
        }

        public void Draw()
        {
            Rect[] positions = getPositions(Link);
            Rect input = positions[0];
            Rect output = positions[1];
            if (Link == null)
            {
                return;
            }
            Handles.DrawBezier(
             input.center + Vector2.left * (output.width / 2 + 10) + Vector2.up * 5,
             output.center + Vector2.right * (output.width / 2 + 10) + Vector2.up * 5,
             input.center + Vector2.left * (output.width / 2 + 10) * 2,
             output.center + Vector2.right * (output.width / 2 + 10) * 2,
             color,
             null,
             2.5f
         );
            if (Handles.Button((input.center + output.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleCap))
            {
                onRemove(this.Link);
            }
        }
    }
}