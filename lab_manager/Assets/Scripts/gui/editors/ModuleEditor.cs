﻿using UnityEngine;
using System;
using Newtonsoft.Json;
using UnityEditor;
using System.Collections.Generic;

namespace LabManager
{
    [System.Serializable]
    public class ModuleEditor : Editor
    {
        public string Key { get; set; }
        public Module module { get; set; }
        [JsonIgnore]
        public Action<string,string,string> OnInputClicked { get; set; }
        [JsonIgnore]
        public Action<string, string, string> OnOutputClicked { get; set; }
        [JsonIgnore]
        public Action<ModuleEditor> OnRemove { get; set; }
        public Rect Rect { get; set; }

        private const int BaseHeight = 65;
        private const int AnchorHeight = 17;
        private const int Width = 200;
        private const int EventYPosBase = 40;
        private bool isDragged;
        private bool isSelected;
        [JsonIgnore]
        private GUIStyle nodeStyle = Styles.ModuleStyle();
        private Texture2D inputImage;
        private Texture2D outputImage;


        public ModuleEditor()
        {
            Rect = new Rect(50, 50, Width, BaseHeight);
        }

        public ModuleEditor(Module module):this()
        {
            this.module = module;
        }

        public void Place(float position)
        {
            Rect = new Rect(position - Rect.width - 10, Rect.y, Rect.width, Rect.height);
        }

        public void DrawWindow(int id)
        {
            GUI.DragWindow(new Rect(0, 0, 10000, 10000));
        }

        public void Draw()
        {
            float height = GetHeight() + 20;
            Rect  = new Rect(Rect.x, Rect.y, Rect.width, height);
            GUI.Box(Rect, "", nodeStyle);
            GUI.Box(new Rect(Rect.x + 7, Rect.y, Rect.width - 14, 20), this.module.Name, Styles.HeaderStyle());
            DrawLinkAnchors(module.InputEvents,true);
            DrawLinkAnchors(module.OutputEvents,false);
        }

      
        public void Drag(Vector2 delta)
        {
            Rect = new Rect(Rect.x + delta.x, Rect.y + delta.y, Rect.width, Rect.height);
        }

        private void DrawLinkAnchors(List<EventInfo> events, bool input)
        {
            int i = 0;
           
            float eventBase = 0;
            float baseYpos = Rect.y + 30;
            float eventSize = Rect.height / events.Count;
            
            foreach (EventInfo e in events)
            {
                float xPos = input ? Rect.x + Rect.width -100 : Rect.x + 80;
                float yPos = Rect.y + EventYPosBase + eventBase;
               
                Rect labelPosition = new Rect(xPos - 20, yPos, 10, 10);
                EditorGUI.LabelField(labelPosition, e.Name, Styles.EventStyle());

                int j = 0;
                foreach (String p in e.Parameters)
                {
                    xPos = input ? Rect.x + Rect.width - 10 : Rect.x -5;
                    DrawAnchor(xPos, yPos + j * (AnchorHeight + 5), Styles.RighLinkAnchor(), input?OnInputClicked:OnOutputClicked, e.Name,p,input);
                    j++;
                }
                i++;
                eventBase += eventSize -15;
            }

            LoadImages();
        }

       
        private void DrawAnchor(float xPos, float yPos,GUIStyle style, Action<string,string,string> onAnchorClicked,string eventName,string param, bool input)
        {
            string imgPath = input ? "circules 16-01.png" : "triangle 16-01.png";
            Texture2D image = (Texture2D)EditorGUIUtility.Load(imgPath);
            Rect linkPoint = new Rect(xPos, yPos, 20 + param.Length, AnchorHeight);
            if (GUI.Button(linkPoint, image, GUIStyle.none))
            {
                onAnchorClicked(this.Key,eventName,param);
            }
            xPos = input ? xPos - 20 : xPos + 20;
            Rect textPosition = new Rect(xPos, yPos, 20 + param.Length, AnchorHeight);
            EditorGUI.LabelField(textPosition, param, Styles.EventStyle());
        }

        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (Rect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                            isSelected = true;
                            nodeStyle = Styles.SelectedModuleStyle();
                        }
                        else
                        {
                            GUI.changed = true;
                            isSelected = false;
                            nodeStyle = Styles.ModuleStyle();
                        }
                    }

                    if (e.button == 1 && Rect.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }

            return false;
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
            genericMenu.ShowAsContext();
        }

        private void OnClickRemoveNode()
        {
            if (OnRemove != null)
            {
                OnRemove(this);
            }
        }

    
        private void LoadImages()
        {
            if (inputImage == null)
            {
                inputImage = (Texture2D)EditorGUIUtility.Load("input16_2.png");
                outputImage = (Texture2D)EditorGUIUtility.Load("output16_2.png");
            }
        }

        private float GetHeight()
        {
            float inputHeight = GetEventsTotalHeight(module.InputEvents);
            float outputHeight = GetEventsTotalHeight(module.OutputEvents);
            return Math.Max(inputHeight, outputHeight);
        }

        private float GetEventsTotalHeight(List<EventInfo> events)
        {
            float height = BaseHeight;
            foreach (EventInfo e in events)
            {
                height += e.Name.Length * 2;
                height += Math.Max(e.Parameters.Count,2) * 30;
            }
            return height;
        }

        internal Rect GetInputAnchorPosition(string eventName, string paramName)
        {
            return GetAnchorPosition(module.InputEvents, eventName, paramName, true);
        }

        internal Rect GetOutputAnchorPosition(string eventName, string paramName)
        {
            return GetAnchorPosition(module.OutputEvents, eventName, paramName, false);
        }

        Rect GetAnchorPosition(List<EventInfo> events, string eventName, string paramName, bool input)
        {
            int i = 0;
            int j = 0;
            foreach (EventInfo e in events)
            {
                j = 0;
                if (e.Name.Equals(eventName))
                {
                    foreach (string p in e.Parameters)
                    {
                        if (p.Equals(paramName))
                        {
                            break;
                        }
                        j++;
                    }
                    break;
                }
                i++;
            }
            float xPos = input ? Rect.x + Rect.width + 12 : Rect.x - 32;
            float yPos = (Rect.y + EventYPosBase + i * (Rect.height / events.Count - 15) + j * (AnchorHeight + 5));
            if (!input)
            {
                yPos = yPos -= 5;
            }
            return new Rect(xPos, yPos, 20 + paramName.Length, AnchorHeight);
        }

    }

}