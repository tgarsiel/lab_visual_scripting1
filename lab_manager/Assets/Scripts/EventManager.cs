﻿using System;
using UnityEngine;
using static LabManager.LabManagerEditor;
using static LabManager.Params;
using static LabManager.TolpologyEditor;

namespace LabManager
{
    public class EventManager
    {
        public void LinkAddedHandler(object sender, EventArgs e)
        {
            Link link = ((LinkArgs)e).Link;
            InputVarData data = LabManagerEditor.GetInput(link);
            data.Vars.StartListening(data.GameObj);
        }

        public void LinkRemovedHandler(object sender, EventArgs e)
        {
            Link link = ((LinkArgs)e).Link;
            InputVarData data = LabManagerEditor.GetInput(link);
            data.Vars.StopListening(data.GameObj);
        }
    }
}
