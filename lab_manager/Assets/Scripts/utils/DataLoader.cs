﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace LabManager
{
    public class DataLoader<T>
    {
        private  string dataPath = "";
        public DataLoader(string path)
        {
            this.dataPath = path;
        }

        public void Save(T o)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            string json = JsonConvert.SerializeObject(o, settings);
            StreamWriter sw = File.CreateText(dataPath);
            sw.Close();
            File.WriteAllText(dataPath, json);
        }

        public T Load()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            //settings.CheckAdditionalContent = false;
            T result = default(T);
            try
            {
                string json = File.ReadAllText(dataPath);
                result = JsonConvert.DeserializeObject<T>(json, settings);
            }
            catch (FileNotFoundException e)
            {
                
            }
            return result;

        }
    }

}
