﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using static LabManager.Params;

namespace LabManager
{
    public class LabManagerEditor : EditorWindow
    {
        private const string Title = "Lab Manager";
        private const string Header = "Build Toplogy";
        private const string DropBoxLabel = "Select Module";
        private List<Module> modules = new List<Module>();
        static TolpologyEditor tolpologyEditor = new TolpologyEditor();
        static EventManager manager = new EventManager();
        static EditorWindow mainWindow;
        private Vector2 offset;
        private Vector2 drag;
        private static DataLoader<TolpologyEditor> editorsLoader;
        private static DataLoader<Topology> topologyLoader;
        private static Dictionary<string, InputVarData> Inputs = new Dictionary<string, InputVarData>();


        void Awake()
        {
            FindModulesWithParams();
            LoadTopology();
            AddEventsHandlers();
        }

        public static EditorWindow GetMainWindow()
        {
            return mainWindow;
        }

        [MenuItem("Window/Lab Manager")]
        public static void ShowWindow()
        {
            mainWindow = GetWindow<LabManagerEditor>(Title);
        }
        void OnGUI()
        {
            GUILayout.Label(Header, EditorStyles.boldLabel);
            DrawGrid(20, 0.2f, Color.gray);
            DrawGrid(100, 0.4f, Color.gray);
            GUIContent content = new GUIContent
            {
                text = DropBoxLabel
            };
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();

            DrawSaveButton();
            DrawModuleSelectionDropdown(content);
            tolpologyEditor.Draw();

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        private void OnModuleAdded(object module)
        {
            ModuleEditor moduleEditor = new ModuleEditor((Module)module);
            tolpologyEditor.AddModule(moduleEditor, "", mainWindow.position.width);
        }

        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            offset += drag * 0.5f;
            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }

        private void DrawSaveButton()
        {
            Rect saveButton = new Rect(GetWidth() - 110, 20, 100, 30);

            if (GUI.Button(saveButton, "Save Toplogy", Styles.SaveButton()))
            {
                Topology topology = BuildToplogy();
                editorsLoader.Save(tolpologyEditor);
                topologyLoader.Save(topology);
            }
        }


        private void DrawModuleSelectionDropdown(GUIContent content)
        {
            GUILayout.BeginArea(new Rect(10, 30, GetWidth() - 150, 50));
            if (EditorGUILayout.DropdownButton(content, FocusType.Keyboard))
            {
                GenericMenu toolsMenu = new GenericMenu();
                foreach (Module module in modules)
                {
                    toolsMenu.AddItem(new GUIContent(module.Name), false, OnModuleAdded, module);
                    toolsMenu.AddSeparator("");
                }
                toolsMenu.DropDown(new Rect(0, 0, 100, 50));
                EditorGUIUtility.ExitGUI();
            }
            GUILayout.EndArea();
        }


        private void FindModulesWithParams()
        {
            MonoBehaviour[] allGameObjects = FindObjectsOfType<MonoBehaviour>();
            Type inputParamsType = typeof(InputParams);
            Type outpuParamsType = typeof(OutputParams);
            foreach (MonoBehaviour o in allGameObjects)
            {
                Type type = o.GetType();
                FieldInfo[] typeInfos = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
                Module module = new Module(type.Name);
                bool paramFound = false;
                foreach (FieldInfo fieldInfo in typeInfos)
                {
                    if (!inputParamsType.IsAssignableFrom(fieldInfo.FieldType) && !outpuParamsType.IsAssignableFrom(fieldInfo.FieldType))
                    {
                        continue;
                    }
                    paramFound = true;
                    InputVars vars = (InputVars)fieldInfo.GetValue(o);
                    Inputs[InputKey(type.Name, vars.GetEventName())] = new InputVarData
                    {
                        GameObj = o.gameObject,
                        Vars = vars
                    };
                    EventInfo info = new EventInfo
                    {
                        Name = vars.GetEventName(),
                        Parameters = vars.GetParams()
                    };
                    if (inputParamsType.IsAssignableFrom(fieldInfo.FieldType))
                    {
                        module.InputEvents.Add(info);
                    }
                    else
                    {
                        module.OutputEvents.Add(info);
                    }
                }
                if (paramFound)
                {
                    modules.Add(module);
                }
            }
        }

        private void LoadTopology()
        {
            string editorDataPath = System.IO.Path.Combine(Application.persistentDataPath, "editors_gui.json");
            string topologyDataPath = System.IO.Path.Combine(Application.persistentDataPath, "modules.json");
            editorsLoader = new DataLoader<TolpologyEditor>(editorDataPath);
            topologyLoader = new DataLoader<Topology>(topologyDataPath);

            TolpologyEditor savedTopology = editorsLoader.Load();

            if (savedTopology == null)
            {
                return;
            }

            foreach (KeyValuePair<string, ModuleEditor> entry in savedTopology.moduleEditors)
            {
                foreach (Module m in modules)
                {
                    if (m.Name.Equals(entry.Value.module.Name))
                    {
                        entry.Value.module = m;
                    }
                }
                tolpologyEditor.AddModule(entry.Value, entry.Key, 0);
            }

            foreach (KeyValuePair<string, LinkEditor> entry in savedTopology.linkEditors)
            {
                Link link = entry.Value.Link;
                if (tolpologyEditor.moduleEditors.ContainsKey(link.Input.ModuleName) && tolpologyEditor.moduleEditors.ContainsKey(link.Output.ModuleName))
                {
                    tolpologyEditor.AddLinkEditor(link.Input, link.Output);
                }
            }
        }

        private void AddEventsHandlers()
        {
            tolpologyEditor.LinkAdded += manager.LinkAddedHandler;
            tolpologyEditor.LinkRemoved += manager.LinkRemovedHandler;
        }


        private static Topology BuildToplogy()
        {
            Topology topology = new Topology();
            Dictionary<String, Module> modules = new Dictionary<String, Module>();
            foreach (KeyValuePair<string, ModuleEditor> entry in tolpologyEditor.moduleEditors)
            {
                modules[entry.Key] = entry.Value.module;

                foreach (KeyValuePair<string, LinkEditor> linkEntry in tolpologyEditor.linkEditors)
                {
                    topology.Links.Add(linkEntry.Value.Link);
                }
            }

            topology.Modules = modules;
            return topology;
        }

        private float GetWidth()
        {
            if (mainWindow == null)
            {
                return 0;
            }
            return mainWindow.position.width;
        }

        private static string InputKey(string module, string eventName)
        {
            return string.Format("input{0}{1}", module, eventName);
        }

        public static InputVarData GetInput(Link link)
        {
            string moduleKey = link.Input.ModuleName;
            string moduleName = tolpologyEditor.moduleEditors[moduleKey].module.Name;
            return Inputs[InputKey(moduleName, link.Input.EventName)];
        }

        public class InputVarData
        {
            public GameObject GameObj;
            public InputVars Vars;
        } 
    }


}
    
