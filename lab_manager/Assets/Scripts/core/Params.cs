﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace LabManager
{
    public class Params
    {
        public class InputVars
        {
            private string eventName;

            public InputVars(string eventName)
            {
                this.eventName = eventName;
            }

            public void StartListening(GameObject o)
            {
                Debug.Log(string.Format("start listening {0}", o.name));
            }

            public void StopListening(GameObject o)
            {
                Debug.Log(string.Format("stop listening {0}", o.name));
            }

            public string GetEventName()
            {
                return eventName;
            }
            public List<string> GetParams()
            {
                List<string> paramList = new List<string>();
                Type type = GetType();
                PropertyInfo[] typeInfos = type.GetProperties();
                foreach (PropertyInfo info in typeInfos)
                {
                    paramList.Add(info.Name);
                }
                FieldInfo[] filedInfos = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
                foreach (FieldInfo info in filedInfos)
                {
                    paramList.Add(info.Name);
                }
                return paramList;
            }
        }

        public class InputParams : InputVars
        {

            public InputParams(string eventName) : base(eventName)
            {

            }

        }

        public class OutputParams : InputVars
        {
            private InputVars vars;

            public OutputParams(string eventName) : base(eventName)
            {

            }
        }
    }
}
