﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LabManager;
using static LabManager.Params;

public class DemoSender : MonoBehaviour
{

    public class MyOutputParams : OutputParams
    {
        public MyOutputParams(string eventName) : base(eventName)
        {

        }
        public string fu;
        public int fi;
    }
    public MyOutputParams MyParams = new MyOutputParams("MyNiceEvent");
    public MyOutputParams MyParams1 = new MyOutputParams("MyNiceEvent1");
    public MyOutputParams MyParams2 = new MyOutputParams("MyNiceEvent2");
  
}
