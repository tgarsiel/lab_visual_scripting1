﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using static LabManager.Params;

namespace LabManager
{
    public class Topology
    {
        public Dictionary<String, Module> Modules { get; set; }
        public List<Link> Links { get; set; }

        public Topology()
        {
            Links = new List<Link>();
        }
    }

   
    public class DataStream
    {

    }

    public class Param
    {
        public string ModuleName;
        public string EventName;
        public string ParamName;
        public string GetName()
        {
            return String.Format("p_{0}_{1}_{2}", this.ModuleName,this.EventName,this.ParamName);
        }
    }

    public class Link
    {
        public Param Input;
        public Param Output;
    }

    public class EventInfo
    {
        public string Name { get; set; }
        public List<String> Parameters { get; set; }
    }
   
    public class Module
    {
        public string Name { get; set; }
        public int ID { get; set; }
        internal List<EventInfo> InputEvents = new List<EventInfo>();
        internal List<EventInfo> OutputEvents = new List<EventInfo>();

        public Module()
        { 
        }
        public Module(string name)
        {
            Name = name;
        }
       
    }


}
